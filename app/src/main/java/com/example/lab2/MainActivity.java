package com.example.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void RGBActivity(View view) {
        Intent intent =  new Intent(this, RGB.class);
        startActivity(intent);
    }
    public void CmykActivity(View view) {
        Intent intent =  new Intent(this, CMYK.class);
        startActivity(intent);
    }
    public void ListaActivity(View view) {
        Intent intent =  new Intent(this, Lista.class);
        startActivity(intent);
    }
}
